//
//  BaseView.h
//  kkjump
//
//  Created by Mr Kelly on 12-3-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
//  MVC View - 单例



#import <Foundation/Foundation.h>
#import "KKSingleton.h"

@interface KKView : KKSingleton

@end

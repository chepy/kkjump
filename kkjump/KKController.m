//
//  KKController.m
//  kkjump
//
//  Created by Mr Kelly on 12-3-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKController.h"

@implementation KKController
@synthesize scene=_scene;


//static KKController *_sharedController = nil;  // Singleton
//
///**
// *  Singleton Design Pattern
// */
//+ (KKController *)shared
//{
//	if (!_sharedController) {
//        
//        _sharedController = [[self alloc] init];  // Singleton init
//	}
//    
//	return _sharedController;
//
//}



/**
 *  执行
 */
-(CCScene *)scene {
    
    if ( !_scene ) {
        CCScene *newScene = [CCScene node];
        
        _scene = newScene;
        
        [_scene addChild: [self index] ];  // index layer
    }
    
    return _scene;
    
}

-(CCNode *)index {
    return [CCLayer node];
}


@end

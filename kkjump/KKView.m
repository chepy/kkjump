//
//  BaseView.m
//  kkjump
//
//  Created by Mr Kelly on 12-3-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKView.h"

@implementation KKView

static KKView *_sharedView = nil;  // Singleton

/**
 *  Singleton Design Pattern
 */
+ (KKView *)shared
{
	if (!_sharedView) {
        
        _sharedView = [[self alloc] init];  // Singleton init
	}
    
	return _sharedView;
    
}

@end

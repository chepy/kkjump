//
//  AppDelegate.h
//  kkjump
//
//  Created by Mr Kelly on 12-2-29.
//  Copyright __MyCompanyName__ 2012年. All rights reserved.
//


#import "BaseController.h"
#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow			*window;
	RootViewController	*viewController;
    
    BaseController      *_baseGameController;
    
}

@property (nonatomic, retain) UIWindow *window;

@end

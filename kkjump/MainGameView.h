//
//  GameLayer.h
//  kkjump
//
//  Created by Mr Kelly on 12-2-29.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"


@interface MainGameLayer : CCLayer {
    CCSprite *_player;
    CCAction *_jumpAction;
}

@property (nonatomic, retain) CCSprite *player;
@property (nonatomic, retain) CCAction *jumpAction;

@end

/**
 *  背景层
 */
@interface BgLayer : CCLayer {
    
}

@end



// Main Object
@interface MainGameView : NSObject {
    
@private
    MainGameLayer *gameLayer;
    
}

+(CCLayer *) view;



@end







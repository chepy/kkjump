//
//  KKSingleton.m
//  kkjump
//
//  Created by Mr Kelly on 12-3-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKSingleton.h"

@implementation KKSingleton

static KKSingleton *_sharedObject = nil;  // Singleton

/**
 *  Singleton Design Pattern
 */
+ (KKSingleton *)shared
{
	if (!_sharedObject) {
        
        _sharedObject = [[self alloc] init];  // Singleton init
	}
    
	return _sharedObject;
    
}

@end

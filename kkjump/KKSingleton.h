//
//  KKSingleton.h
//  kkjump
//
//  Created by Mr Kelly on 12-3-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
//   用于单健模式


#import <Foundation/Foundation.h>

#import "KKObject.h"


@interface KKSingleton : KKObject

+(KKSingleton *) shared;

@end

//
//  GameLayer.m
//  kkjump
//
//  Created by Mr Kelly on 12-2-29.
//  Copyright 2012年 __MyCompanyName__. All rights reserved.
//
#import "cocos2d.h"
#import "MainGameView.h"

@implementation MainGameView


+(CCLayer *) view {
    
    CCLayer *_view = [BgLayer node];
    [_view addChild:[MainGameLayer node]];
    
    return _view;
}


@end





@implementation BgLayer {
    
}
-(id) init {
    self = [super init];
    
    if ( self ) {
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        
        CCSprite *bgSprite = [CCSprite spriteWithFile:@"scene0/background.png"];
        
        bgSprite.position = ccp( winSize.width / 2, winSize.height / 2 );
        
        [self addChild:bgSprite];
    }
    
    return self;
}


@end



#pragma mark 游戏主层 Main Game Layer

@implementation MainGameLayer

@synthesize player = _player;
@synthesize jumpAction = _jumpAction;


-(id)init {
    
    if ( self = [super init] ) {

        // 动画
        // Frame Cache
        [CCTexture2D PVRImagesHavePremultipliedAlpha:YES];
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"player.plist"];
        
        // Spritesheet
        
        CCSpriteBatchNode *spritesheet = [CCSpriteBatchNode batchNodeWithFile:@"player.pvr.ccz"];
        [self addChild:spritesheet];
        
        // 帧列表
        int playerFrameCount = 7;  // 帧数
        NSMutableArray *playerFrames = [NSMutableArray array];
        for ( int i=0; i < playerFrameCount - 1; i++ ) {
            // Add Frame to Sprite
            [playerFrames addObject:
                    [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                     [NSString stringWithFormat:@"player_%d.png", i]]];
        }
        
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        

        self.player = [CCSprite spriteWithSpriteFrameName:@"player_2.png"];
        [self.player setTextureRect:CGRectMake(0, 0, 65, 80)];
        
        self.player.position = ccp( winSize.width / 2, winSize.height / 2 );
        [spritesheet addChild: self.player];
        
        // Start Animation
//        CCAnimation *jumpAnim = [CCAnimation animationWithFrames:playerFrames delay:0.1f];
//        
//        // OK , action 
//        CGSize winSize = [[CCDirector sharedDirector] winSize];
//        self.player = [CCSprite spriteWithSpriteFrameName:@"player_0.png"];
//        self.player.position = ccp( winSize.width / 2, winSize.height / 2 );
//        self.jumpAction = [ CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:jumpAnim restoreOriginalFrame:NO]];
//        
//        [self.player runAction:self.jumpAction];
//        [spritesheet addChild:self.player];  // 性能提升。。
        
        
//        CGSize winSize = [[CCDirector sharedDirector] winSize];
//        
//        self.player = [CCSprite spriteWithFile:@"player_0.png"];
//        self.player.position = ccp( winSize.width / 2, winSize.height / 2 );
//        
//        [self addChild:self.player];  
//        
//        //id jumpAction = [CCJumpTo actionWithDuration:2 position:ccp(winSize.width / 2, winSize.height /2) height:50 jumps:5];
//        
//        //[player runAction:jumpAction];        
//        [self schedule:@selector(jump:)];
    }

    
    return self;
}

-(void)jump:(id)sender {
    NSLog(@"Stop Jump");
    
    self.player.position = ccp( self.player.position.x, self.player.position.y + 0.1f );
    
    [self.player stopAllActions];

}


-(void)dealloc {
    self.player = nil;
    self.jumpAction = nil;
    [super dealloc];
}
@end

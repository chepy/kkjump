//
//  BaseController.h
//  kkjump
//
//  Created by Mr Kelly on 12-3-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//  

//
#import "cocos2d.h"
#import <Foundation/Foundation.h>
#import "KKSingleton.h"



@interface KKController : KKSingleton {

    CCScene *_scene;
}

@property (nonatomic, retain) CCScene *scene;



-(CCScene *)scene;

-(CCNode *)index;



@end
